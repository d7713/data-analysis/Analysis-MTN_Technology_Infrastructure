# MTN Technology Infrastructure - Analysis

### By: Andrew Wairegi

## Description
To analyse the MTN technology infrastructure. Using python. The aim
is to find out the areas in the MTN cell tower coverage that need to be improved,
or more cell towers added there. However, there is no specific information about the
the areas where these cell towers are. So I went with cell tower IDs.

[Open notebook]

## Setup/Installation instructions
1. Create a folder on your computer
2. Set it up as an empty repository (git init)
3. Clone this repository (using git clone https://...)
4. Upload the notebook from that folder to google drive
5. Open it
6. Upload the appropriate data files from the folder to the google collab (upload section)
7. Run the notebook

## Known Bugs
There are no known issues / bugs

## Technologies Used
1. Python - The programming language
2. Numpy - An Arithmetic Package
3. Pandas - A Data Analysis Package

<br>

### License
Copyright © 2021 **Andrew Wairegi**
<br>
You are free to view, but not copy.

[Open notebook]: https://gitlab.com/d7713/data-analysis/Analysis-MTN_Technology_Infrastructure/-/blob/main/MTN_Technology_Infrastructure_Analysis.ipynb
